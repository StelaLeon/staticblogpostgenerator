val gitStatus = os.proc("git", "status").call()
val gitBranchLines = os.proc("git", "branch").call().out.lines()
val otherBranches = gitBranchLines.filter(_.startsWith(" ")).map(_.drop(2))


for (branch <- otherBranches) os.proc("git", "branch", "-D", branch).call()
val gitBranchLines = os.proc("git", "branch").call().out.lines()


/*
While everything we did here using subprocesses could also have been done in-process, e.g. using a library
like JGit, it is sometimes easier use a command-line tool you are already familiar with rather than having to
  learn an entirely new programmatic API.*/


val url = "https://api.github.com/repos/lihaoyi/mill/releases"
os.proc("curl", url).call(stdout = os.pwd / "github.json")
os.proc("ls", "-lh", "github.json").call().out.text()

//Interactive and Streaming Subprocesses with SPAWN
/**
 * os.proc(command: os.Shellable*)
    .spawn(cwd: Path = null,
    env: Map[String, String] = null,
    stdin: os.ProcessInput = os.Pipe,
    stdout: os.ProcessOutput = os.Pipe,
    stderr: os.ProcessOutput = os.Inherit,
    mergeErrIntoOut: Boolean = false,
    propagateEnv: Boolean = true): os.SubProcess
 */
/**
 *  os.proc.spawn takes a similar set of arguments as os . proc . call , but instead of returning a completed
    os . CommandResult , it instead returns an os . SubProcess object. This represents a subprocess that runs in
    the background while your program continues to execute, and you can interact with it via its stdin , stdout
    and stderr streams.
 */

val sub = os.proc("python", "-u", "-c", "while True: print(eval(raw_input()))").spawn()

sub.stdin.writeLine("1 + 2 + 4")
sub.stdin.writeLine("'1' + '2' + '4'")
sub.stdin.flush()
sub.stdin.flush()
sub.stdout.readLine()

/**
 * This spawn usage pattern is handy in a few cases:
 * 1. process is slow to initialize
 * 2. in-memory state that will be lost via respawning a new sub-process every time
 * 3. the work is untrusted => you want to delegate security concerns to the OS to run it as a subprocess
 *      so you can take advantage of the sandboxing and security features provided by the OS.
**/

{
{
  val gitLog = os.proc("git", "log").spawn()
  val grepAuthor = os.proc("grep", "Author: ").spawn(stdin = gitLog.stdout)
  val output = grepAuthor.stdout.lines().distinct
}

{
  val download = os.proc("curl", "https://api.github.com/repos/lihaoyi/mill/releases").spawn()
  val base64 = os.proc("base64").spawn(stdin = download.stdout)
  val gzip = os.proc("gzip").spawn(stdin = base64.stdout)

  val upload = os.proc("curl", "-X", "PUT", "-d", "@-", "https://httpbin.org/anything").spawn(stdin = gzip.stdout)
  val contentLength = upload.stdout.lines().filter(_.contains("Content-Length"))
}