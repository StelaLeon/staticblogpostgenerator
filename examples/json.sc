import scala.collection.mutable

val jsonString = os.read(os.pwd / "examples/ammonite-releases.jsonases.json")

val data = ujson.read(jsonString)

sealed trait Value
case class Str(value: String) extends Value
case class Obj(value: mutable.LinkedHashMap[String, Value]) extends Value
case class Arr(value: mutable.ArrayBuffer[Value]) extends Value
case class Num(value: Double) extends Value
sealed trait Bool extends Value
case object False extends Bool
case object True extends Bool
case object Null extends Value


def traverse(v: ujson.Value): Iterable[String] = v match {
  case a: ujson.Arr => a.arr.map(traverse).flatten
  case o: ujson.Obj => o.obj.values.map(traverse).flatten
  case s: ujson.Str => Seq(s.str)
  case _ => Nil
}


/**
 * To convert a JSON structure into a case class , there are a few steps:
1. Define a case class representing the fields and types you expect to be present in the JSON
2. Define an implicit upickle . default . ReadWriter for that case class
3. Use upickle . default . read to deserialize the JSON structure.
 */

case class Author(login: String, id: Int, site_admin: Boolean)
implicit val authorRW: upickle.default.ReadWriter[Author] = upickle.default.macroRW
val author = upickle.default.read[Author](data(0)("author"))

upickle.default.read[Map[String, Author]]("""{
"haoyi": {"login": "lihaoyi", "id": 1337, "site_admin": true},
"bot": {"login": "ammonite-bot", "id": 31337, "site_admin": false}
}""")


implicit val pathRw = upickle.default.readwriter[String].bimap[os.Path](
  p => p.toString,
  s => os.Path(s)
)

class Foo (val i : Int, val s : String)
implicit val fooRW =  upickle.default.readwriter[ujson.Value].bimap[Foo](
  foo => ujson.Obj("i" -> foo.i, "s" -> foo.s),
  value => new Foo(value("i").num.toInt, value("s").str)
)


val foo = new Foo(1337, "mooo")

val serialized = upickle.default.write(foo)

assert(serialized == """{"i":1337,"s":"mooo"}""")

val deserialized = upickle.default.read[Foo](serialized)

assert(foo.i == deserialized.i)
assert(foo.s == deserialized.s)
