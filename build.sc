import $ivy.`com.lihaoyi::scalatags:0.9.1`, scalatags.Text.all._
import $ivy.`com.atlassian.commonmark:commonmark:0.13.1`
import mill._

def mdNameToHtml(name: String) = name.replace(" ", "-").toLowerCase + ".html"

val postInfo = interp.watchValue {
  os.list(os.pwd / "posts")
    .map { p =>
      val s"$prefix-$suffix.md" = p.last
      (prefix, suffix, p)
    }
    .sortBy(_._1.toInt)
}
/*val bootstrapCss = link(
  rel := "stylesheet",
  href := "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.css"
)*/

val bootstrapCss = T{
  os.write(
    T.dest / "bootstrap.css",
    requests.get("https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.css").text()
  )
  PathRef(T.dest / "bootstrap.css")
}

def puppeteer = T{
  os.proc("npm", "install", "puppeteer@4.0.1").call(cwd = T.dest, stderr = os.Pipe)
  PathRef(T.dest)
}
def pdfize = T.source(os.pwd / "pdfize.js")


def renderMarkdown(s: String) = {
  val parser = org.commonmark.parser.Parser.builder().build()
  val document = parser.parse(s)
  val renderer = org.commonmark.renderer.html.HtmlRenderer.builder().build()
  renderer.render(document)
}

object post extends Cross[PostModule](postInfo.map(_._1):_*)

class PostModule(number: String) extends Module{

  val Some((_, suffix, path)) = postInfo.find(_._1 == number)
  def srcPath = T.source(path)

  def preview = T{
    val firstPara = os.read.lines(srcPath().path).takeWhile(_.nonEmpty).mkString("\n")
    renderMarkdown(firstPara)
  }

  def render = T{
    os.write(
      T.dest /  mdNameToHtml(suffix),
      doctype("html")(
        html(
          head(link(rel := "stylesheet", href := "../bootstrap.css")),
          body(
            h1(a("Blog", href := "../index.html"), " / ", suffix),
            raw(renderMarkdown(os.read(srcPath().path)))
          )
        )
      )
    )
    PathRef(T.dest / mdNameToHtml(suffix))
  }

  def pdf = T {
    for(p <- os.list(puppeteer().path)) os.copy.over(p, T.dest / p.last)
    os.copy(bootstrapCss().path, T.dest / "bootstrap.css")
    os.makeDir(T.dest / "post")
    val htmlPath = T.dest / "post" / render().path.last
    os.copy(render().path, htmlPath)

    val localPdfize = T.dest / pdfize().path.last

    os.copy.over(pdfize().path, localPdfize)
    val s"$baseName.html" = htmlPath.last
    val pdfPath = T.dest / s"$baseName.pdf"
    os.proc("node", localPdfize, htmlPath, pdfPath)
      .call(cwd = T.dest)
    PathRef(pdfPath)
  }

}

def links = T.input{ postInfo.map(_._2) }
val previews = T.sequence(postInfo.map(_._1).map(post(_).preview))
val posts = T.sequence(postInfo.map(_._1).map(post(_).render))

def index = T{
  os.write(
    T.dest / "index.html",
    doctype("html")(
      html(
        head(link(rel := "stylesheet", href := "bootstrap.css")),
        body(
          h1("Blog"),
          for ((suffix, preview) <- links().zip(previews()))
            yield frag(
              h2(a(suffix, href := ("post/" + mdNameToHtml(suffix)))),
              raw(preview)
            )
        )
      )
    )
  )
  PathRef(T.dest / "index.html")
}
def dist = T {
  for (post <- posts()) {
    os.copy(post.path, T.dest / "post" / post.path.last, createFolders = true)
  }
  os.copy(index().path, T.dest / "index.html")
  os.copy(bootstrapCss().path, T.dest / "bootstrap.css")
  PathRef(T.dest)
}

def push(targetGitRepo: String = "") = T.command{
  for(p <- os.list(dist().path)) os.copy(p, T.dest / p.last)
//  os.proc("git", "init").call(cwd = T.dest)
  os.proc("git", "add", "-A").call(cwd = T.dest)
  os.proc("git", "commit", "-am", ".").call(cwd = T.dest)
  os.proc("git", "push").call(cwd = T.dest)
}

val pdfFiles = T.sequence(postInfo.map(_._1).map(post(_).pdf))

def pdfs = T {
  for (pdf <- pdfFiles()) os.copy.into(pdf.path, T.dest)
  PathRef(T.dest)
}

